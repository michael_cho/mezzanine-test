from __future__ import unicode_literals

from django.db import models, connection


class AuthUser(models.Model):
    username = models.CharField(max_length=200)
    email = models.CharField(max_length=200)
    is_staff = models.BooleanField()
    is_active = models.BooleanField()
    date_joined = models.DateTimeField('date joined')

    class Meta:
        db_table = 'auth_user'

    def groups(self):
        with connection.cursor() as c:
            c.execute("""SELECT g.name
            FROM auth_group AS g
             JOIN auth_user_groups ag ON ag.group_id = g.id
             WHERE ag.user_id = {user_id}
             ORDER BY g.name DESC
             """.format(user_id=self.id))
            groups = [group[0] for group in c]

        return groups