from django.shortcuts import render
from .models import AuthUser


def index(request):
    users = AuthUser.objects.order_by('-date_joined')[:5]
    context = {'users': users}
    return render(request, 'community/index.html', context)